// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)

const me = {
  firstName: 'Matt',
  lastName: 'Schneider',
  'favorite food': 'Rosita\'s',
  bestFriend: {
    firstName: 'Trevor',
    lastName: 'Schneider',
    'favorite food': 'Goldfish Crackers',
  }
};

// 2. console.log best friend's firstName and your favorite food
console.log(me.bestFriend.firstName);
console.log(me['favorite food']);

// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X

const row1 = ['-', 'O', '-'];
const row2 = ['-', 'X', 'O'];
const row3 = ['X', '-', 'X'];

const ticTacToeBoard = [row1, row2, row3];

// 4. After the array is created, 'O' claims the top right square.
// Update that value.
row1[2] = 'O';

// 5. Log the grid to the console.
console.log(row1);
console.log(row2);
console.log(row3);


// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test
const goodEmail = 'matts@uw.edu';
const badEmail = 'todd@ ';

const emailRegex = /^\w+@\w+\.\w+$/;
console.log(emailRegex.test(goodEmail));
console.log(emailRegex.test(badEmail));


// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';
const initialDate = new Date(assignmentDate);


// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.
const dueDate = new Date(
  initialDate.getFullYear(),
  initialDate.getMonth(),
  initialDate.getDate() + 7
);


// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const padMonth = (dueDate.getMonth() + 1).toString().padStart(2, '0');
const padDay = dueDate.getDate().toString().padStart(2, '0');

const dateTime = `${dueDate.getFullYear()}-${padMonth}-${padDay}`;
const monthDayYear = `${months[dueDate.getMonth()]} ${dueDate.getDate()}, ${dueDate.getFullYear()}`;
const tag = `<time datetime="${dateTime}">${monthDayYear}</time>`;

// 10. log this value using console.log
console.log(tag);